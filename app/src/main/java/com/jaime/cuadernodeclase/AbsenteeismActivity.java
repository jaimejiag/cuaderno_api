package com.jaime.cuadernodeclase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jaime.cuadernodeclase.adapters.AbsenteeismAdapter;
import com.jaime.cuadernodeclase.models.Absenteeism;
import com.jaime.cuadernodeclase.utils.AbsenteeismResult;
import com.jaime.cuadernodeclase.utils.RestClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class AbsenteeismActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String URL_API = "absenteeism";
    public static final int ADD_CODE = 100;
    public static final int OK = 1;

    FloatingActionButton fabAdd;
    TextView txvDate;
    RecyclerView recyclerView;
    AbsenteeismAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absenteeism);

        fabAdd = (FloatingActionButton) findViewById(R.id.fab_add_absenteeism);
        fabAdd.setOnClickListener(this);
        txvDate = (TextView) findViewById(R.id.txv_date);
        recyclerView = (RecyclerView) findViewById(R.id.rv_absenteeism);
        mAdapter = new AbsenteeismAdapter(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);

        DateFormat format = new SimpleDateFormat("EEEE, yyyy-MM-dd");
        Date today = new Date();
        txvDate.setText(format.format(today));

        downloadAbsenteeism();
    }


    private void downloadAbsenteeism() {
        final ProgressDialog progress = new ProgressDialog(this);
        RestClient.get(URL_API, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Connecting . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progress.dismiss();
                AbsenteeismResult result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), AbsenteeismResult.class);

                if (result != null)
                    if (result.getCode()) {
                        mAdapter.set(result.getAbsenteeism());
                        mAdapter.showTodayAbsenteeism();
                        message = "Connection OK";
                    } else
                        message = result.getMessage();
                else
                    message = "Null data";

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (view == fabAdd) {
            Intent i = new Intent(this, AddAbsenteeismActivity.class);
            startActivityForResult(i, ADD_CODE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Absenteeism absenteeism = new Absenteeism();

        if (requestCode == ADD_CODE)
            if (resultCode == OK) {
                absenteeism.setId(data.getIntExtra("id", 1));
                absenteeism.setStudent(data.getStringExtra("student"));
                absenteeism.setNoAttendance(data.getStringExtra("no_attendance"));
                absenteeism.setWork(data.getStringExtra("work"));
                absenteeism.setAttitude(data.getStringExtra("attitude"));
                absenteeism.setObservation(data.getStringExtra("observation"));
                absenteeism.setDate(data.getStringExtra("date"));
                mAdapter.add(absenteeism);
            }
    }
}
