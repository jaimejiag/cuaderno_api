package com.jaime.cuadernodeclase;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jaime.cuadernodeclase.adapters.StudentsAdapter;
import com.jaime.cuadernodeclase.interfaces.ClickListener;
import com.jaime.cuadernodeclase.models.Student;
import com.jaime.cuadernodeclase.utils.RecyclerTouchListener;
import com.jaime.cuadernodeclase.utils.RestClient;
import com.jaime.cuadernodeclase.utils.StudentResult;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class StudentsActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String URL_API = "student";
    public static final String MAIL = "mail";
    public static final int ADD_CODE = 100;
    public static final int UPDATE_CODE = 200;
    public static final int OK = 1;

    FloatingActionButton fabAdd;
    RecyclerView recyclerView;
    StudentsAdapter mAdapter;
    int positionClicked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);

        fabAdd = (FloatingActionButton) findViewById(R.id.fab_add);
        fabAdd.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.rv_students);
        mAdapter = new StudentsAdapter(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Intent emailIntent = new Intent(getApplicationContext(), EmailActivity.class);
                emailIntent.putExtra(MAIL, mAdapter.getAt(position).getEmail());
                startActivity(emailIntent);
            }

            @Override
            public void onLongClick(View view, int position) {
                showPopup(view, position);
            }
        }));

        downloadStudents();
    }


    private void downloadStudents() {
        final ProgressDialog progress = new ProgressDialog(this);
        RestClient.get(URL_API, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Connecting . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progress.dismiss();
                StudentResult result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), StudentResult.class);

                if (result != null)
                    if (result.getCode()) {
                        mAdapter.set(result.getStudent());
                        message = "Connection OK";
                    } else
                        message = result.getMessage();
                else
                    message = "Null data";

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (view == fabAdd) {
            Intent i = new Intent(this, AddStudentActivity.class);
            startActivityForResult(i, ADD_CODE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Student student = new Student();

        if (requestCode == ADD_CODE)
            if (resultCode == OK) {
                student.setId(data.getIntExtra("id", 1));
                student.setName(data.getStringExtra("name"));
                student.setSurname(data.getStringExtra("surname"));
                student.setAdress(data.getStringExtra("adress"));
                student.setCity(data.getStringExtra("city"));
                student.setZip_code(data.getStringExtra("zip_code"));
                student.setPhone(data.getStringExtra("phone"));
                student.setEmail(data.getStringExtra("email"));
                mAdapter.add(student);
            }

        if (requestCode == UPDATE_CODE)
            if (resultCode == OK) {
                student.setId(data.getIntExtra("id", 1));
                student.setName(data.getStringExtra("name"));
                student.setSurname(data.getStringExtra("surname"));
                student.setAdress(data.getStringExtra("adress"));
                student.setCity(data.getStringExtra("city"));
                student.setZip_code(data.getStringExtra("zip_code"));
                student.setPhone(data.getStringExtra("phone"));
                student.setEmail(data.getStringExtra("email"));

                mAdapter.modifyAt(student, positionClicked);
            }
    }


    private void showPopup(View v, final int position) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.getMenuInflater().inflate(R.menu.popup_change, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.modify_student:
                        modify(mAdapter.getAt(position));
                        positionClicked = position;
                        return true;

                    case R.id.delete_student:
                        confirm(mAdapter.getAt(position).getId(), mAdapter.getAt(position).getName(), position);
                        return true;

                    case R.id.add_absenteeism:
                        String student = mAdapter.getAt(position).getName() + " " +
                                mAdapter.getAt(position).getSurname();
                        Intent intent = new Intent(StudentsActivity.this, AddAbsenteeismActivity.class);
                        intent.putExtra("student", student);
                        startActivity(intent);
                        return true;

                    case R.id.show_absenteeism:
                        Intent i = new Intent(StudentsActivity.this, AbsenteeismActivity.class);
                        startActivity(i);
                        return true;

                    default:
                        return false;
                }
            }
        });

        popup.show();
    }


    private void modify(Student s) {
        Intent i = new Intent(this, UpdateStudentActivity.class);
        i.putExtra("student", s);
        startActivityForResult(i, UPDATE_CODE);
    }


    private void confirm(final int idStudent, String name, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(name + "\n¿Quieres eliminar al estudiante?")
                .setTitle("Eliminar")
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        connection(idStudent, position);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.show();
    }


    private void connection(int id, final int position) {
        final ProgressDialog progress = new ProgressDialog(this);

        RestClient.delete(URL_API + "/" + id, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progress.dismiss();
                StudentResult result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), StudentResult.class);

                if (result != null)
                    if (result.getCode()) {
                        message = "Estudiante eliminado";
                        mAdapter.removeAt(position);
                    } else
                        message = "Error eliminando al estudiante:\nEstado: " + result.getStatus() + "\n" +
                                result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
