package com.jaime.cuadernodeclase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jaime.cuadernodeclase.models.Student;
import com.jaime.cuadernodeclase.utils.RestClient;
import com.jaime.cuadernodeclase.utils.StudentResult;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class AddStudentActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String URL = "student";
    public static final int OK = 1;

    private EditText edtName;
    private EditText edtSurname;
    private EditText edtAdress;
    private EditText edtCity;
    private EditText edtZipCode;
    private EditText edtPhone;
    private EditText edtEmail;
    private Button btnAdd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        edtName = (EditText) findViewById(R.id.edt_name);
        edtSurname = (EditText) findViewById(R.id.edt_surname);
        edtAdress = (EditText) findViewById(R.id.edt_adress);
        edtCity = (EditText) findViewById(R.id.edt_city);
        edtZipCode = (EditText) findViewById(R.id.edt_zipcode);
        edtPhone = (EditText) findViewById(R.id.edt_phone);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        btnAdd = (Button) findViewById(R.id.btn_add);

        btnAdd.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Student s = new Student();

        if (v == btnAdd) {
             if (edtName.getText().toString().isEmpty() || edtSurname.getText().toString().isEmpty())
                Toast.makeText(this, "Aseguresé de que los campos 'nombre' y 'apellidos' no están vacíos",
                        Toast.LENGTH_SHORT).show();
            else {
                 s.setName(edtName.getText().toString());
                 s.setSurname(edtSurname.getText().toString());
                 s.setAdress(edtAdress.getText().toString());
                 s.setCity(edtCity.getText().toString());
                 s.setZip_code(edtZipCode.getText().toString());
                 s.setPhone(edtPhone.getText().toString());
                 s.setEmail(edtEmail.getText().toString());
                connection(s);
            }
        }
    }


    private void connection(final Student s) {
        final ProgressDialog progress = new ProgressDialog(this);
        RequestParams params = new RequestParams();
        params.put("name", s.getName());
        params.put("surname", s.getSurname());
        params.put("adress", s.getAdress());
        params.put("city", s.getCity());
        params.put("zip_code", s.getZip_code());
        params.put("phone", s.getPhone());
        params.put("email", s.getEmail());

        RestClient.post(URL, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progress.dismiss();
                StudentResult result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), StudentResult.class);

                if (result != null) {
                    if (result.getCode()) {
                        message = "Estudiante añadido";
                        Intent i = new Intent();
                        Bundle mBundle = new Bundle();
                        mBundle.putInt("id", result.getLast());
                        mBundle.putString("name", s.getName());
                        mBundle.putString("surname", s.getSurname());
                        mBundle.putString("adress", s.getAdress());
                        mBundle.putString("city", s.getCity());
                        mBundle.putString("zip_code", s.getZip_code());
                        mBundle.putString("phone", s.getPhone());
                        mBundle.putString("email", s.getEmail());
                        i.putExtras(mBundle);
                        setResult(OK, i);
                        finish();
                    } else
                        message = "Error añadiendo al estudiante:\n" + result.getMessage();
                } else {
                    message = "Null data";
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
