package com.jaime.cuadernodeclase.interfaces;

import android.view.View;

/**
 * Created by jaime on 18/02/2017.
 */

public interface ClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
