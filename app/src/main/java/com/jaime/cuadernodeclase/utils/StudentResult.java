package com.jaime.cuadernodeclase.utils;

import com.jaime.cuadernodeclase.models.Student;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jaime on 18/02/2017.
 */

public class StudentResult implements Serializable {
    boolean code;
    int status;
    String message;
    ArrayList<Student> sites;
    int last;

    public boolean getCode() {return code;}

    public void setCode(boolean code) {
        this.code = code;
    }

    public int getStatus() {return status;}

    public void setStatus(int status) {this.status = status;}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) { this.message = message; }

    public ArrayList<Student> getStudent() {
        return sites;
    }

    public void setStudent(ArrayList<Student> sites) { this.sites = sites; }

    public int getLast() { return last; }

    public void setLast(int last) { this.last = last; }
}
