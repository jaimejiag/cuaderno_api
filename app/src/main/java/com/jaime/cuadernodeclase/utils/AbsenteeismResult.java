package com.jaime.cuadernodeclase.utils;

import com.jaime.cuadernodeclase.models.Absenteeism;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jaime on 18/02/2017.
 */

public class AbsenteeismResult implements Serializable {
    boolean code;
    int status;
    String message;
    ArrayList<Absenteeism> sites;
    int last;

    public boolean getCode() {return code;}

    public void setCode(boolean code) {
        this.code = code;
    }

    public int getStatus() {return status;}

    public void setStatus(int status) {this.status = status;}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) { this.message = message; }

    public ArrayList<Absenteeism> getAbsenteeism() {
        return sites;
    }

    public void setAbsenteeism(ArrayList<Absenteeism> sites) { this.sites = sites; }

    public int getLast() { return last; }

    public void setLast(int last) { this.last = last; }
}
