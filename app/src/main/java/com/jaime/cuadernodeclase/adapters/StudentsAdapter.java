package com.jaime.cuadernodeclase.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaime.cuadernodeclase.R;
import com.jaime.cuadernodeclase.models.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jaime on 18/02/2017.
 */

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.MyViewHolder> {
    private List<Student> studentList;
    private LayoutInflater inflater;


    public StudentsAdapter(Context context) {
        this.studentList = new ArrayList<>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.student_item, parent, false);
        return new MyViewHolder(rootView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Student feed = studentList.get(position);

        holder.txvName.setText(feed.getName());
        holder.txvSurname.setText(feed.getSurname());
        holder.txvAdress.setText(feed.getAdress());
        holder.txvCity.setText(feed.getCity());
        holder.txvZipCode.setText(feed.getZip_code());
        holder.txvPhone.setText(feed.getPhone());
        holder.txvEmail.setText(feed.getEmail());
    }


    @Override
    public int getItemCount() {
        return studentList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txvName;
        TextView txvSurname;
        TextView txvAdress;
        TextView txvCity;
        TextView txvZipCode;
        TextView txvPhone;
        TextView txvEmail;

        public MyViewHolder(View itemView) {
            super(itemView);
            txvName = (TextView) itemView.findViewById(R.id.txv_name);
            txvSurname = (TextView) itemView.findViewById(R.id.txv_surname);
            txvAdress = (TextView) itemView.findViewById(R.id.txv_adress);
            txvCity = (TextView) itemView.findViewById(R.id.txv_city);
            txvZipCode = (TextView) itemView.findViewById(R.id.txv_zipcode);
            txvPhone = (TextView) itemView.findViewById(R.id.txv_phone);
            txvEmail = (TextView) itemView.findViewById(R.id.txv_email);
        }
    }


    public void set(ArrayList<Student> studentList) {
        this.studentList = studentList;
        notifyItemRangeChanged(0, studentList.size() - 1);
    }


    public Student getAt(int position) {
        return studentList.get(position);
    }


    public void add(Student student) {
        studentList.add(student);
        notifyItemInserted(studentList.size() - 1);
        notifyItemRangeChanged(0, studentList.size() - 1);
    }


    public void modifyAt(Student student, int position) {
        studentList.set(position, student);
        notifyItemChanged(position);
    }


    public void removeAt(int position) {
        studentList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, studentList.size() - 1);

    }
}
