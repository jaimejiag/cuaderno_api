package com.jaime.cuadernodeclase.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaime.cuadernodeclase.R;
import com.jaime.cuadernodeclase.models.Absenteeism;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jaime on 18/02/2017.
 */

public class AbsenteeismAdapter extends RecyclerView.Adapter<AbsenteeismAdapter.MyViewHolder> {
    private List<Absenteeism> absenteeismList;
    private LayoutInflater inflater;


    public AbsenteeismAdapter(Context context) {
        this.absenteeismList = new ArrayList<>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public AbsenteeismAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.absenteeism_item, parent, false);
        return new AbsenteeismAdapter.MyViewHolder(rootView);
    }


    @Override
    public void onBindViewHolder(AbsenteeismAdapter.MyViewHolder holder, int position) {
        Absenteeism feed = absenteeismList.get(position);

        holder.txvStudent.setText(feed.getStudent());
        holder.txvNoAttendance.setText(feed.getNoAttendance());
        holder.txvWork.setText(feed.getWork());
        holder.txvAttitude.setText(feed.getAttitude());
        holder.txvObservation.setText(feed.getObservation());
    }


    @Override
    public int getItemCount() {
        return absenteeismList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txvStudent;
        TextView txvNoAttendance;
        TextView txvWork;
        TextView txvAttitude;
        TextView txvObservation;

        public MyViewHolder(View itemView) {
            super(itemView);
            txvStudent = (TextView) itemView.findViewById(R.id.txv_student);
            txvNoAttendance = (TextView) itemView.findViewById(R.id.txv_noAttendance);
            txvWork = (TextView) itemView.findViewById(R.id.txv_work);
            txvAttitude = (TextView) itemView.findViewById(R.id.txv_attitude);
            txvObservation = (TextView) itemView.findViewById(R.id.txv_observation);
        }
    }


    public void set(ArrayList<Absenteeism> absenteeismList) {
        this.absenteeismList = absenteeismList;
        notifyItemRangeChanged(0, absenteeismList.size() - 1);
    }


    public void add(Absenteeism absenteeism) {
        absenteeismList.add(absenteeism);
        notifyItemInserted(absenteeismList.size() - 1);
        notifyItemRangeChanged(0, absenteeismList.size() - 1);
    }


    public void showTodayAbsenteeism() {
        ArrayList<Absenteeism> filterList = new ArrayList<>();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(new Date());

        for (int i = 0; i < absenteeismList.size(); i++) {
            if (absenteeismList.get(i).getDate().equalsIgnoreCase(date))
                filterList.add(absenteeismList.get(i));
        }

        absenteeismList.clear();
        absenteeismList.addAll(filterList);
        notifyDataSetChanged();
    }
}
