package com.jaime.cuadernodeclase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jaime.cuadernodeclase.models.Absenteeism;
import com.jaime.cuadernodeclase.models.Student;
import com.jaime.cuadernodeclase.utils.AbsenteeismResult;
import com.jaime.cuadernodeclase.utils.RestClient;
import com.jaime.cuadernodeclase.utils.StudentResult;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class AddAbsenteeismActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String URL = "absenteeism";
    public static final int OK = 1;

    private EditText edtStudent;
    private EditText edtObservation;
    private Spinner spNoAttendance;
    private Spinner spWork;
    private Spinner spAttitude;
    private Button btnAdd;

    private String mNoAttendance;
    private String mWork;
    private String mAttitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_absenteeism);

        edtStudent = (EditText) findViewById(R.id.edt_student);
        edtObservation = (EditText) findViewById(R.id.edt_observation);
        spNoAttendance = (Spinner) findViewById(R.id.sp_noAttendance);
        spWork = (Spinner) findViewById(R.id.sp_work);
        spAttitude = (Spinner) findViewById(R.id.sp_attitude);
        btnAdd = (Button) findViewById(R.id.btn_add_absenteeism);

        Intent intent = getIntent();

        if (intent.getStringExtra("student") != null) {
            edtStudent.setText(intent.getStringExtra("student"));
            edtStudent.setEnabled(false);
        }


        btnAdd.setOnClickListener(this);
        buildSpinners();
    }


    @Override
    public void onClick(View v) {
        Absenteeism a = new Absenteeism();

        if (v == btnAdd) {
            if (edtStudent.getText().toString().isEmpty() || edtObservation.getText().toString().isEmpty())
                Toast.makeText(this, "Aseguresé de que los campos 'Nombre alumno' y 'Observaciones' no están vacíos",
                        Toast.LENGTH_SHORT).show();
            else {
                a.setStudent(edtStudent.getText().toString());
                a.setObservation(edtObservation.getText().toString());
                a.setNoAttendance(mNoAttendance);
                a.setWork(mWork);
                a.setAttitude(mAttitude);

                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                String date = format.format(new Date());
                a.setDate(date);

                connection(a);
            }
        }
    }


    private void connection(final Absenteeism a) {
        final ProgressDialog progress = new ProgressDialog(this);
        RequestParams params = new RequestParams();
        params.put("student", a.getStudent());
        params.put("no_attendance", a.getNoAttendance());
        params.put("work", a.getWork());
        params.put("attitude", a.getAttitude());
        params.put("observation", a.getObservation());
        params.put("date", a.getDate());

        RestClient.post(URL, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progress.dismiss();
                AbsenteeismResult result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), AbsenteeismResult.class);

                if (result != null) {
                    if (result.getCode()) {
                        message = "Falta añadida";
                        Intent i = new Intent();
                        Bundle mBundle = new Bundle();
                        mBundle.putInt("id", result.getLast());
                        mBundle.putString("student", a.getStudent());
                        mBundle.putString("no_attendance", a.getNoAttendance());
                        mBundle.putString("work", a.getWork());
                        mBundle.putString("attitude", a.getAttitude());
                        mBundle.putString("observation", a.getObservation());
                        mBundle.putString("date", a.getDate());
                        i.putExtras(mBundle);
                        setResult(OK, i);
                        finish();
                    } else
                        message = "Error añadiendo la falta:\n" + result.getMessage();
                } else {
                    message = "Null data";
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void buildSpinners() {
        ArrayAdapter<String> noAttendanceAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.no_attendance));
        spNoAttendance.setAdapter(noAttendanceAdapter);
        spNoAttendance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    mNoAttendance = " ";
                else if (i == 1)
                    mNoAttendance = "I";
                else if (i == 2)
                    mNoAttendance = "J";
                else
                    mNoAttendance = "R";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> workAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.work));
        spWork.setAdapter(workAdapter);
        spWork.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    mWork = " ";
                else if (i == 1)
                    mWork = "B";
                else if (i == 2)
                    mWork = "R";
                else
                    mWork = "M";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> attitudeAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.attitude));
        spAttitude.setAdapter(attitudeAdapter);
        spAttitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    mAttitude = " ";
                else if (i == 1)
                    mAttitude = "P";
                else
                    mAttitude = "N";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
