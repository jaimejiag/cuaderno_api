package com.jaime.cuadernodeclase.models;

import java.io.Serializable;

/**
 * Created by jaime on 18/02/2017.
 */

public class Student implements Serializable {
    private int id;
    private String name;
    private String surname;
    private String adress;
    private String city;
    private String zip_code;
    private String phone;
    private String email;


    public Student(int id, String name, String surname, String adress, String city, String zip_code, String phone, String email) {
        super();
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.adress = adress;
        this.city = city;
        this.zip_code = zip_code;
        this.phone = phone;
        this.email = email;
    }


    public Student(String name, String surname, String adress, String city, String zip_code, String phone, String email) {
        super();
        this.name = name;
        this.surname = surname;
        this.adress = adress;
        this.city = city;
        this.zip_code = zip_code;
        this.phone = phone;
        this.email = email;
    }


    public Student() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
