package com.jaime.cuadernodeclase.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jaime on 18/02/2017.
 */

public class Absenteeism implements Serializable {
    private int id;
    private String student;
    private String noAttendance;
    private String work;
    private String attitude;
    private String observation;
    private String date;


    public Absenteeism(int id, String student, String noAttendance, String work, String attitude, String observation, String date) {
        this.id = id;
        this.student = student;
        this.noAttendance = noAttendance;
        this.work = work;
        this.attitude = attitude;
        this.observation = observation;
        this.date = date;
    }


    public Absenteeism(String student, String noAttendance, String work, String attitude, String observation, String date) {
        this.student = student;
        this.noAttendance = noAttendance;
        this.work = work;
        this.attitude = attitude;
        this.observation = observation;
        this.date = date;
    }


    public Absenteeism() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getNoAttendance() {
        return noAttendance;
    }

    public void setNoAttendance(String noAttendance) {
        this.noAttendance = noAttendance;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getAttitude() {
        return attitude;
    }

    public void setAttitude(String attitude) {
        this.attitude = attitude;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
