### Instrucciones de uso de la App ###
* La activity principal muestra una lista de los alumnos. 
* Para enviar un email pulsa al alumno.
* La API sólo permite enviar emails desde una cuenta de **portadaalta.info**.
* Para desplegar las opciones de editar y eliminar alumno, mantén pulsado al alumno.
* Para añadir una falta a un alumno, o ver todas las faltas del día actual, mantén pulsado a un alumno para desplegar el menú con las opciones.
* Si al acceder a la activity de faltas no se visualiza ninguna, es debido a que se muestra sólo aquellas faltas añadidas en el día actual, por lo que puede haber datos en la base de datos pero no visualizarse. Añade alguna falta para ver el resultado.
* El archivo **slim.zip** contiene toda la configuración de la API. El archivo se encuentra en la raíz del repositorio.